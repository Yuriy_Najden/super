<?php

require_once('classes/AmoCrm.php');

/**
 * @param $data
 * @return string
 */
function clearData($data) {
    return addslashes(strip_tags(trim($data)));
}
/*
$order = clearData($_POST['order']);

$name = $order['name'];
$email = $order['email'];
$phone = 'нет';
$package = $order['description'];
*/


/***********                          Platon                                     **********/

$data = var_export($_POST, true);
$log = __DIR__ . '/callbackPlaton.log';
file_put_contents($log, $data. "\n\n", FILE_APPEND);// логируем ответ от процессинга;


$fullname = explode(" ", $_POST['name']);
$name = '';
for ($i=1; $i<count($fullname); $i++){
    $name = $name.$fullname[$i]." "; // Имя плательщика;
}

$order = $_POST['order']; // номер ордера(так же возвращатся GET-ом при редиректк плательщика после оплаты на Success_page), так как Вы его не передаете в запросе - генерируется процессингом.
$package = $_POST['description']; // Описание услуги
$email = $_POST['email']; // почта плательщика 
$phone = $_POST['ext2'];
$amount = $_POST['amount']." UAH"; // фактическая сумма списания в гривне
$price = $_POST['ext1']; // стоимость услуги

/***********                          Platon                                     **********/

if(!empty($name) && !empty($email)) {

    // Save user in crm
    $amoCrm = new AmoCrm([
        'USER_LOGIN' => 'ak@superludi.com',
        'USER_HASH'  => '466c4eb808b94813a639f66f9dfe7d1b'
    ], 'superludi');

    $lead = $amoCrm->storeLead( $package, 21776883 );

    var_dump($lead);

    $leadId = $lead['response']['leads']['add'][0]['id'];

    $amoCrm->storeContact($name, $leadId, $email, $phone);


}
?>