<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:type" content="video.movie" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="/img/og.png" />

    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

    <meta name="msapplication-TileColor" content="#ffffff">

    <meta name="theme-color" content="#ffffff">

    <title>Специальное предложение</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="css/style.css?v2.0"/>

</head>
<body>
<div class="wrapper">
    <div class="loader" id="loader"></div>
    <header>
        <div class="container">
            <div class="row">
                <div class="content col-md-12">
                    <div class="logo">
                        <svg xmlns="http://www.w3.org/2000/svg" width="" height="" viewBox="0 0 649.2097778320312 167.1999969482422" fill="none" preserveAspectRatio="xMidYMid meet">
                            <path d="M11.8 23.6C5.3 23.6 0 18.3 0 11.8C0 5.3 5.3 0 11.8 0C15.1 0 18.1 1.3 20.3 3.7L17.3 6.6C15.8 5.1 13.9 4.2 11.8 4.2C7.6 4.2 4.2 7.6 4.2 11.8C4.2 16 7.6 19.4 11.8 19.4C13.9 19.4 15.9 18.6 17.3 17L20.3 19.9C18.1 22.3 15.1 23.6 11.8 23.6ZM78.6 23.6C72.1 23.6 66.8 18.3 66.8 11.8C66.8 5.3 72.1 0 78.6 0C81.9 0 84.9 1.3 87.1 3.7L84.1 6.6C82.6 5.1 80.7 4.2 78.6 4.2C74.4 4.2 71 7.6 71 11.8C71 16 74.4 19.4 78.6 19.4C80.7 19.4 82.7 18.6 84.1 17L87.1 19.9C84.9 22.3 81.9 23.6 78.6 23.6Z" transform="translate(49 46.4) scale(2)" fill="white"></path>
                            <path d="M4.5 0.6L10.1 13L15.5 0.6H20L12.1 17.9C11.7 18.7 11.4 19.4 10.9 20C10.5 20.6 9.99999 21.2 9.39999 21.7C8.79999 22.2 8.2 22.6 7.5 22.8C6.8 23.1 5.9 23.2 5 23.2C4.3 23.2 3.7 23.1 3.2 23C2.7 22.9 2.3 22.8 2 22.6L1.1 22.1L0.5 21.6L2.89999 18.6C3.09999 18.8 3.49999 19.1 3.89999 19.3C4.29999 19.5 4.7 19.6 5.3 19.6C5.9 19.6 6.4 19.4 6.8 19.1C7.2 18.8 7.6 18.3 8 17.6L0 0.5H4.5V0.6ZM41.6 0.6V22.7H37.6V4.1H28.2V22.7H24V0.6H41.6ZM53.8 19.1H64.6V22.7H49.8V0.6H64.2V4.1H53.8V9.6H63.5V13H53.8V19.1ZM71.7 0.6H79.1C80.3 0.6 81.4 0.7 82.4 1C83.4 1.2 84.3 1.6 85 2.1C85.7 2.6 86.3 3.3 86.7 4.1C87.1 4.9 87.3 5.9 87.3 7C87.3 8.2 87.1 9.2 86.7 10C86.3 10.8 85.7 11.5 84.9 12C84.2 12.5 83.3 12.9 82.3 13.2C81.3 13.4 80.2 13.6 79.1 13.6H75.9V22.7H71.7V0.6ZM75.8 10.3H78.7C80 10.3 81.1 10.1 81.9 9.6C82.7 9.1 83.2 8.3 83.2 7.1C83.2 6.5 83.1 6 82.9 5.6C82.7 5.2 82.4 4.9 82 4.7C81.6 4.5 81.2 4.3 80.6 4.2C80.1 4.1 79.5 4 78.9 4H76V10.3H75.8ZM92.8 22.7H88.4L97.3 0.6H101.5L110.5 22.7H106L99.4 6.1L92.8 22.7ZM119.2 0.6V9.8H122.9C123.1 8.3 123.5 7 124.1 5.8C124.8 4.6 125.6 3.6 126.6 2.7C127.6 1.8 128.8 1.2 130.2 0.700001C131.5 0.200001 133 0 134.6 0C136.3 0 137.9 0.300001 139.3 0.800001C140.7 1.4 142 2.1 143 3.2C144.1 4.2 144.9 5.4 145.5 6.8C146.1 8.2 146.4 9.8 146.4 11.5C146.4 13.2 146.1 14.8 145.5 16.2C144.9 17.6 144.1 18.9 143 19.9C141.9 20.9 140.7 21.7 139.3 22.3C137.9 22.9 136.3 23.2 134.6 23.2C133 23.2 131.6 23 130.3 22.5C129 22 127.8 21.3 126.8 20.5C125.8 19.6 124.9 18.6 124.3 17.4C123.6 16.2 123.2 14.9 123 13.4H119.3V22.7H115.2V0.6H119.2ZM127.1 11.6C127.1 12.7 127.3 13.8 127.6 14.7C128 15.7 128.5 16.5 129.1 17.2C129.7 17.9 130.5 18.5 131.4 18.9C132.3 19.3 133.3 19.5 134.5 19.5C135.7 19.5 136.6 19.3 137.6 18.9C138.5 18.5 139.3 17.9 140 17.2C140.6 16.5 141.1 15.6 141.5 14.7C141.9 13.7 142 12.7 142 11.6C142 10.5 141.8 9.5 141.5 8.5C141.1 7.5 140.6 6.7 140 6C139.4 5.3 138.6 4.7 137.6 4.3C136.7 3.9 135.7 3.7 134.5 3.7C133.3 3.7 132.4 3.9 131.4 4.3C130.5 4.7 129.7 5.3 129.1 6C128.5 6.7 128 7.5 127.6 8.5C127.3 9.5 127.1 10.5 127.1 11.6Z" transform="translate(230.6 46.6) scale(2)" fill="white"></path>
                            <path d="M3.5 22.1V26.4H0V18.5H3.39999L10.4 0H14.4L21.3 18.5H24.8V26.4H21.3V22.1H3.5ZM7.5 18.5H16.9L12.2 4.5L7.5 18.5ZM31.7 16.3L42.4 0H47.1V22.1H43V5.8H42.9L32.3 22.1H27.5V0H31.6V16.3H31.7Z" transform="translate(525.8 47.8) scale(2)" fill="white"></path>
                            <path d="M35.4 0L41 14.8L53.2 4.7L50.5 20.3L66.1 17.7L56.1 29.9L70.9 35.4L56.1 41L66.1 53.2L50.5 50.5L53.2 66.1L41 56.1L35.4 70.9L29.9 56.1L7.6 83.6L20.3 50.5L4.7 53.2L14.8 41L0 35.4L14.8 29.9L4.7 17.7L20.3 20.3L17.7 4.7L29.9 14.8L35.4 0Z" transform="scale(2)" fill="#C9002C"></path>
                            <path d="M11.8 23.6C5.3 23.6 0 18.3 0 11.8C0 5.3 5.3 0 11.8 0C15.1 0 18.1 1.3 20.3 3.7L17.3 6.6C15.8 5.1 13.9 4.2 11.8 4.2C7.6 4.2 4.2 7.6 4.2 11.8C4.2 16 7.6 19.4 11.8 19.4C13.9 19.4 15.9 18.6 17.3 17L20.3 19.9C18.1 22.3 15.1 23.6 11.8 23.6Z" transform="translate(49 46.2) scale(2)" fill="white"></path>
                            <path d="M1.58015 5.14909H2.49766V0.8H4.07782V0H0V0.8H1.58015V5.14909Z" transform="translate(628 38) scale(2)" fill="white"></path>
                            <path d="M5.04163 5.14909H5.91545V1.01091H5.93002L7.47376 5.14909H8.11456L9.68015 1.01091H9.69471V5.14909H10.6049V0H9.16314L7.83785 3.65818H7.81601L6.47615 0H5.04163V5.14909Z" transform="translate(628 38) scale(2)" fill="white"></path>
                        </svg>
                    </div>
                    <div class="phone-wrapper">
                        <a href="tel:0800210791" class="phone">0 800 210 791</a>
                        <p>бесплатно по Украине</p>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <main>
        <div class="container">
            <div class="row">
                <div class="content col-md-12">
                    <div class="item pay-wrapper">
                        <!--<p class="save">СОХРАНИТЕ $50</p>-->
                        <h3 class="sale-name">Специальное предложение</h3>
                        <h4>Два онлайн-курса за</h4>
                        <div class="price-button">
                            <div class="prise">
                                <p class="old-prise">$750</p>
                                <p class="new-price">$249</p>
                            </div>
                            <a href="#" class="button anim-hover" data-toggle="modal" data-target="#myForm">Купить</a>
                        </div>
                    </div>

                    <div class="item men">
                        <div class="pic">
                            <img src="img/fedoriv-pic.jpg" alt="">
                            <div class="veil"></div>
                        </div>
                        <p class="type">Онлайн-курс</p>
                        <h3 class="name">Папа бренда </h3>
                        <!--<p class="time"></p>
                        <a href="https://brandfather.superludi.com/" target="_blank">Подробнее о курсе</a>-->
                    </div>

                    <div class="item men">
                        <div class="pic">
                            <img src="img/nina-pic.png" alt="">
                            <div class="veil"></div>
                        </div>
                        <p class="type">Онлайн-курс</p>
                        <h3 class="name">Персональный бренд как инструмент роста</h3>
                        <!--<p class="time">5 часовой авторский курс</p>
                        <a href="https://personalbrand.superludi.com/" target="_blank">Подробнее о курсе</a>-->
                    </div>
                </div>
            </div>
        </div>
    </main>

    <section class="pre-footer">
        <div class="container">
            <div class="row">
                <div class="content col-md-12">
                    <div class="soc">
                        <a href="https://www.facebook.com/superludi.official/" target="_blank">
                            <svg width="18" height="32" viewBox="0 0 18 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M17.0091 0.00624203L12.7696 0C8.00719 0 4.92861 3.09147 4.92861 7.87537V11.507H0.665707C0.298845 11.507 0 11.7979 0 12.1587V17.4207C0 17.7798 0.298845 18.0724 0.665707 18.0724H4.92861V31.3483C4.92861 31.7075 5.22746 32 5.59602 32H11.1555C11.524 32 11.8229 31.7075 11.8229 31.3483V18.0724H16.8063C17.1749 18.0724 17.4725 17.7798 17.4725 17.4207L17.4754 12.1587C17.4754 11.9864 17.404 11.8207 17.2795 11.6972C17.1549 11.5752 16.9857 11.507 16.808 11.507H11.8229V8.42841C11.8229 6.94905 12.1834 6.19792 14.1512 6.19792L17.0074 6.19626C17.376 6.19626 17.6731 5.90372 17.6731 5.54459V0.659991C17.6731 0.298785 17.376 0.00790658 17.0091 0.00624203Z" fill="white"/>
                            </svg>
                        </a>
                        <a href="https://www.instagram.com/superludi.official/" target="_blank">
                            <svg width="31" height="31" viewBox="0 0 31 31" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="0" y="0" width="31" height="31">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0 0H30.4309V30.4H0V0Z" transform="translate(0.0732422 0.599976)" fill="white"/>
                                </mask>
                                <g mask="url(#mask0)">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M22.0429 0H8.40153C3.76858 0 0 3.76331 0 8.3895V22.0107C0 26.6369 3.76858 30.4 8.40153 30.4H22.0429C26.6758 30.4 30.4444 26.636 30.4444 22.0107V8.3895C30.4444 3.76331 26.6758 0 22.0429 0ZM27.7454 22.0107C27.7454 25.1496 25.1886 27.7028 22.0451 27.7028H8.40373C5.26025 27.7028 2.70337 25.1496 2.70337 22.0107V8.38948C2.70337 5.25058 5.26025 2.6972 8.40373 2.6972H22.0451C25.1886 2.6972 27.7454 5.25058 27.7454 8.38948V22.0107Z" transform="translate(0.0578613 0.600037)" fill="white"/>
                                </g>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M7.65331 0C3.43292 0 0 3.42816 0 7.6416C0 11.8561 3.43292 15.2843 7.65331 15.2843C11.8728 15.2843 15.3066 11.8561 15.3066 7.6416C15.3066 3.42816 11.8728 0 7.65331 0ZM7.65367 12.6519C4.88722 12.6519 2.6355 10.4051 2.6355 7.64161C2.6355 4.87921 4.88634 2.63062 7.65367 2.63062C10.421 2.63062 12.6718 4.87921 12.6718 7.64161C12.6718 10.4051 10.4201 12.6519 7.65367 12.6519Z" transform="translate(7.12427 7.48572)" fill="white"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M1.76625 0C1.30206 0 0.846499 0.1872 0.518977 0.515903C0.188847 0.843203 0 1.29878 0 1.76397C0 2.22736 0.189649 2.68234 0.518977 3.01104C0.845896 3.33834 1.30206 3.52714 1.76625 3.52714C2.23184 3.52714 2.6858 3.33834 3.01492 3.01104C3.34425 2.68234 3.5323 2.22656 3.5323 1.76397C3.5323 1.29878 3.34425 0.843203 3.01492 0.515903C2.6872 0.1872 2.23184 0 1.76625 0Z" transform="translate(21.2537 6.30994)" fill="white"/>
                            </svg>
                        </a>
                    </div>
                    <div class="info">
                        <h3>Поддержка</h3>
                        <a href="mailto:info@superludi.com" target="_blank">info@superludi.com</a>
                        <a href="tel:0800210791">0 800 210 791 (Украина)</a>
                        <p class="smal">Бесплатно по Украине</p>
                        <a href="tel:74997031623">+7(499) 703 16 23 (СНГ)</a>
                    </div>
                    <div class="card">
                        <h3>Мы принимаем</h3>
                        <div class="img-wrapper">
                            <img src="img/visa.png" alt="">
                            <img src="img/master.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <p>© 2018 Суперлюди   . Все права защищены</p>
    </footer>
</div>

<?php include ('components/popup.php');?>


<!--[if IE]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 7]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script>
<![endif]-->
<!--[if lt IE 8]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!--<script src="js/jquery-3.2.1.min.js"></script>-->

<script src="js/bootstrap.min.js"></script>

<script src="js/jquery.maskedinput.min.js"></script>
<script src="js/jquery.countdown.js"></script>

<script src="js/js.js?v2.0"></script>

<script type="text/javascript">
	$("#payform").submit( function() { //Change
        
        var th = $(this);
        $.ajax({
            type: "POST",
            url: "send.php", //Change
            data: th.serialize()
        });
    });
</script>

</body>
</html>
