$(function() {

    var now = new Date().getMonth();
    var nowDate = new Date().getDate();
    var nowHovers  = new Date().getHours();
    var nowMonth = 1;
    var timerMonth = 0;

    if(now < 1){
        nowMonth = 1;
    }else {
        nowMonth = now + 1;
    }

    if( nowDate >= 29 && nowHovers >= 11 ){
        $('.form-wrapper').css({'display' : 'none'});
        $('.sale-off-wrapper').css({'display' : 'block'});
        $('.wrapper').css({'display' : 'flex', 'height':'100vh', 'align-items':'center', 'min-height':'800px'});
    } else if( nowDate > 29 && nowDate < 28){
        $('.form-wrapper').css({'display' : 'none'});
        $('.sale-off-wrapper').css({'display' : 'block'});
        $('.wrapper').css({'display' : 'flex', 'height':'100vh', 'align-items':'center', 'min-height':'800px'});
    }

    // PHONE MASK

    $("input[type=tel]").mask("+38(999) 999-99-99");

    //
    $('.anim-hover').hover( function () {
       $('.men img').toggleClass('pic-scale');
       $('.men .veil').toggleClass('pic-scale');
    });
    //

    $('#timer').countdown('2018/09/29 11:00', function (event) {
        $(this).html(event.strftime(''
            + '<div class="v-center">%D<span class="after-number">Дней</span></div>'
            + '<div class="v-center">%H<span class="after-number">Часов</span></div>'
            + '<div class="v-center">%M<span class="after-number">Минут</span></div>'
            + '<div class="v-center">%S<span class="after-number">Секунд</span></div>'));
    });


});

$(window).on('load', function () {

    $('#loader').fadeOut(500);

});