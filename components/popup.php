<div class="modal fade myform" id="myForm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <svg width="12px" height="12px" viewBox="0 0 12 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" xmlns:="">
                        <!-- Generator: Sketch 50.2 (55047) - http://www.bohemiancoding.com/sketch -->
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="close-button" fill="#cdcdcd">
                                <polygon id="Fill-1" points="11.199 1.921 10.078 0.8 5.602 5.28 1.121 0.8 0 1.921 4.48 6.402 0 10.878 1.121 11.999 5.602 7.519 10.078 11.999 11.199 10.878 6.719 6.402"></polygon>
                            </g>
                        </g>
                    </svg>
                </button>
                <h5>Онлайн-курсы</h5>
                <h4 class="modal-title" id="myModalLabel">Папа бренда + Персональный бренд как инструмент роста</h4>
                <div class="steps">
                    <p>
                        <span>
                           1.Личные данные
                        </span>
                        <svg width="16" height="11" viewBox="0 0 16 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M15.4698 4.95283L10.5718 0L9.48958 1.09434L13.0809 4.72608H0V6.27384H13.0809L9.48958 9.90566L10.5718 11L15.4698 6.04717C15.5721 5.94368 15.7489 5.75596 16 5.48402C15.7435 5.22795 15.5668 5.05088 15.4698 4.95283Z" fill="#cdcdcd"></path>
                        </svg>
                    </p>
                    <p class="gray">
                        2.Оплата
                        <svg width="16" height="11" viewBox="0 0 16 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M15.4698 4.95283L10.5718 0L9.48958 1.09434L13.0809 4.72608H0V6.27384H13.0809L9.48958 9.90566L10.5718 11L15.4698 6.04717C15.5721 5.94368 15.7489 5.75596 16 5.48402C15.7435 5.22795 15.5668 5.05088 15.4698 4.95283Z" fill="#cdcdcd"></path>
                        </svg>
                    </p>
                    <p class="gray">3.Доступ к видео</p>
                </div>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="Имя" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="Телефон" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="email" required>
                    </div>
                    <button type="submit" class="button">Продолжить</button>
                    <p class="small">После подтверждения оплаты в течение 12 часов Вам откроется доступ к курсам</p>
                </form>
            </div>
        </div>
    </div>
</div>